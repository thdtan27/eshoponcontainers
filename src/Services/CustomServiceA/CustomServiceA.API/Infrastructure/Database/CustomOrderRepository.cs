﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CustomServiceA.API.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CustomServiceA.API.Infrastructure.Database
{
    public class CustomOrderRepository : ICustomOrderRepository
    {
        private readonly CustomOrdersContext _context;


        public CustomOrderRepository(IOptions<CustomOrderDbSettings> setting)
        {
            _context = new CustomOrdersContext(setting);
            
        }

    
        public async Task AddCustomOrderAsync(CustomOrder order)
        {
            await _context.CustomOrders.InsertOneAsync(order);
        }


        public async Task<List<CustomOrder>> GetCustomOrdersAsync()
        {

            return await _context.CustomOrders.Find(new BsonDocument()).ToListAsync();
        }

        public async Task<long> GetNumberOfCustomOrdersAsync()
        {
            return await _context.CustomOrders.Find(new BsonDocument()).CountAsync();
        }

       
    }
}
