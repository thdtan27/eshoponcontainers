﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CustomServiceA.API.Models;
using MongoDB.Bson;

namespace CustomServiceA.API.Infrastructure.Database
{
    public interface ICustomOrderRepository
    {
        Task<List<CustomOrder>> GetCustomOrdersAsync();

        Task<long> GetNumberOfCustomOrdersAsync();

        Task AddCustomOrderAsync(CustomOrder order);
    }
}
