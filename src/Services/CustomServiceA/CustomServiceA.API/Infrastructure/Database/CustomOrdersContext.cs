﻿using System;
using CustomServiceA.API.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace CustomServiceA.API.Infrastructure.Database
{
    public class CustomOrdersContext
    {
        private readonly IMongoDatabase _database = null;

        public CustomOrdersContext(IOptions<CustomOrderDbSettings> setting)
        {
            //var client = new MongoClient(setting.Value.DbConnectionString);
            var client = new MongoClient("mongodb://nosql.data");
            if (client != null)
            {
                _database = client.GetDatabase("CustomOrderDb");
            }
        }

        public IMongoCollection<CustomOrder> CustomOrders
        {
            get
            {
                return _database.GetCollection<CustomOrder>("CustomOrders");
            }
        }
    }
}
