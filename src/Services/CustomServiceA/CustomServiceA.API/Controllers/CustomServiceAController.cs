﻿using CustomServiceA.API.Infrastructure.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.BuildingBlocks.EventBus.Abstractions;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace CustomServiceA.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CustomServiceAController : ControllerBase
    {
        private readonly IEventBus _eventBus;

        private readonly ICustomOrderRepository _repo;

        public CustomServiceAController(IEventBus eventBus, ICustomOrderRepository repo)
        {
            _eventBus = eventBus;
            _repo = repo;
        }

        //GET api/v1/[controller]/
        [HttpGet]
        public IActionResult Get()
        {
            return Ok("Hello world");
        }

        [HttpGet("GetCustomOrders")]
        public async Task<IActionResult> GetCustomOrders()
        {
            var orders = await _repo.GetCustomOrdersAsync();
            if (orders != null)
            {
                return Ok(orders);
            }

            return Ok("No custom orders!!!");
        }

        [HttpGet("GetCount")]
        public async Task<IActionResult> GetCount()
        {
          
            return Ok(await _repo.GetNumberOfCustomOrdersAsync());
        }



    }
}
