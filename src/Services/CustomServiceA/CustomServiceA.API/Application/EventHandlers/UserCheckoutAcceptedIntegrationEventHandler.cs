﻿using System;
using System.Threading.Tasks;
using CustomService.API.IntegrationEvents.Events;
using Microsoft.eShopOnContainers.BuildingBlocks.EventBus.Abstractions;
using Microsoft.eShopOnContainers.Services.CustomService.API;
using Microsoft.Extensions.Logging;
using Serilog.Context;
using System.Threading;
using CustomServiceA.API.Infrastructure.Database;
using CustomServiceA.API.Models;

namespace CustomService.API.IntegrationEvents.EventHandling
{
    public class UserCheckoutAcceptedIntegrationEventHandler : IIntegrationEventHandler<UserCheckoutAcceptedIntegrationEvent>
    {
        private readonly ILogger<UserCheckoutAcceptedIntegrationEventHandler> _logger;
        private readonly ICustomOrderRepository _repository;

        public UserCheckoutAcceptedIntegrationEventHandler(ICustomOrderRepository repository, ILogger<UserCheckoutAcceptedIntegrationEventHandler> logger)
        {
            _logger = logger;
            _repository = repository;
        }

        public async Task Handle(UserCheckoutAcceptedIntegrationEvent @event)
        {
            _logger.LogTrace("----- UserCheckoutAcceptedIntegrationEvent is processed by custom service : {RequestId}", @event.RequestId);

            _logger.LogInformation("----- UserCheckoutAcceptedIntegrationEvent is processed by custom service : {RequestId}", @event.RequestId);
            long orderNumber = await _repository.GetNumberOfCustomOrdersAsync() + 1;
            int orderId = Convert.ToInt32(orderNumber);
            CustomOrder order = new CustomOrder(
                @event.UserId,
                @event.UserName,
                @event.City,
                @event.Street,
                @event.State,
                @event.Country,
                @event.ZipCode,
                @event.CardNumber,
                @event.CardHolderName,
                @event.CardExpiration,
                @event.CardSecurityNumber,
                @event.CardTypeId,
                @event.Buyer,
                @event.RequestId,
                @event.Basket);

            order.OrderNumber = orderId;
            order.ShippingMethod = "Custom Shipping Service";
            order.ShippinFee = 100;
            decimal total = 0;
            foreach(var item in @event.Basket.Items) 
            {
                total += item.UnitPrice;
            }
            order.Total = total;
            order.TotalOrder = total + order.ShippinFee;

            await _repository.AddCustomOrderAsync(order);

            _logger.LogInformation("----- Custom Order is created!!! -----");

            //for (int i = 0; i < 100; i++)
            //{
            //    await Task.Factory.StartNew(() => Thread.Sleep(1000));
            //}

        }
    }
}
