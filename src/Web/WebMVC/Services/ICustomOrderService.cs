﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.eShopOnContainers.WebMVC.ViewModels;

namespace Microsoft.eShopOnContainers.WebMVC.Services
{
    public interface ICustomOrderService
    {
        Task<List<CustomOrder>> GetCustomOrder();

        Task AddOrder(CustomOrder order);
    }
}
