﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.eShopOnContainers.WebMVC.ViewModels;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Microsoft.eShopOnContainers.WebMVC.Services
{
    public class CustomOrderService : ICustomOrderService
    {
        private HttpClient _httpClient;
        private readonly string _remoteServiceBaseUrl;
        private readonly IOptions<AppSettings> _settings;

        public CustomOrderService(HttpClient httpClient, IOptions<AppSettings> settings)
        {
            _httpClient = httpClient;
            _settings = settings;
            _remoteServiceBaseUrl = $"{settings.Value.CustomOrderUrl}/api/v1/CustomServiceA/GetCustomOrders";
            
        }

        public Task AddOrder(CustomOrder order)
        {
            throw new NotImplementedException();
        }

        public async Task<List<CustomOrder>> GetCustomOrder()
        {
            var uri = "http://docker.for.mac.localhost:5888/api/v1/CustomServiceA/GetCustomOrders";

            var responseString = await _httpClient.GetStringAsync(uri);

            var response = JsonConvert.DeserializeObject<List<CustomOrder>>(responseString);

            return response;

        }
    }
}
