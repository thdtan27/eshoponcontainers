﻿using System;
namespace Microsoft.eShopOnContainers.WebMVC.ViewModels
{
    public class CustomOrder
    {

        public string Id { get; set; }

        public string UserId { get; }

        public string UserName { get; }

        public int OrderNumber { get; set; }

        public string City { get; set; }

        public string Street { get; set; }

        public string State { get; set; }

        public string Country { get; set; }

        public string ZipCode { get; set; }

        public string CardNumber { get; set; }

        public string CardHolderName { get; set; }

        public DateTime CardExpiration { get; set; }

        public string CardSecurityNumber { get; set; }

        public int CardTypeId { get; set; }

        public string Buyer { get; set; }

        public Guid RequestId { get; set; }

        public string ShippingMethod { get; set; }

        public decimal ShippinFee { get; set; }

        public decimal TotalOrder { get; set; }

        public decimal Total { get; set; }

    }
}
