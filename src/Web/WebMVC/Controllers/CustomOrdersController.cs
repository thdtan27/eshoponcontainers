﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebMVC.Services.ModelDTOs;
using Microsoft.eShopOnContainers.WebMVC.Services;
using Microsoft.eShopOnContainers.WebMVC.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace WebMVC.Controllers
{

    public class CustomOrderController : Controller
    {
        private ICustomOrderService _customOrderSvc;
        private readonly IIdentityParser<ApplicationUser> _appUserParser;

        public CustomOrderController(ICustomOrderService orderSvc, IIdentityParser<ApplicationUser> appUserParser)
        {
            _appUserParser = appUserParser;
            _customOrderSvc = orderSvc;
        }

        public async Task<IActionResult> Index()
        {
            var vm = await _customOrderSvc.GetCustomOrder();
            return View(vm);
        }

    }
}
