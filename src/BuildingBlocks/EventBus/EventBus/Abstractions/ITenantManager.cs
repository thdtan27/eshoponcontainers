﻿using Microsoft.eShopOnContainers.BuildingBlocks.EventBus.Events;
using System.Collections.Generic;
using System;

namespace Microsoft.eShopOnContainers.BuildingBlocks.EventBus.Abstractions
{
    public interface ITenantManager
    {
        void AddServiceEvent(string service, string eventName);

        List<String> GetServicesByEvent(string eventName);

        bool IsEventRegisteredToService(string eventName, string serviceName);

        bool IsEventCustomized(string eventName);

    }
}
