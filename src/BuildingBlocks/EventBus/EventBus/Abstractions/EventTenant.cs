﻿using Microsoft.eShopOnContainers.BuildingBlocks.EventBus.Events;
using System;

namespace Microsoft.eShopOnContainers.BuildingBlocks.EventBus.Abstractions
{
    public class EventTenant
    {
        public string Service { get; set; }

        public string EventName { get; set; }

    }
}
