﻿using Microsoft.eShopOnContainers.BuildingBlocks.EventBus.Events;
using System;
using System.Collections.Generic;

namespace Microsoft.eShopOnContainers.BuildingBlocks.EventBus.Abstractions
{
    public class TenantManager : ITenantManager
    {
        public List<EventTenant> EventTenants { get; set; }

        public TenantManager()
        {
            EventTenants = new List<EventTenant>();

            AddServiceEvent("CustomServiceA", "UserCheckoutAcceptedIntegrationEvent");
        }
        public void AddServiceEvent(string service, string eventName)
        {
            EventTenant eventTenant = new EventTenant();
            eventTenant.Service = service;
            eventTenant.EventName = eventName;
            EventTenants.Add(eventTenant);
        }

        public List<String> GetServicesByEvent(string eventName)
        {
            List<String> services = new List<string>();

            foreach (var eventTenant in EventTenants)
            {
                if (eventTenant.EventName == eventName)
                {
                    services.Add(eventTenant.Service);
                }
            }

            return services;
        }

        public bool IsEventRegisteredToService(string eventName, string serviceName)
        {
            bool found = false;

            for (int i = 0; i < EventTenants.Count; i++)
            {
                if (String.Compare(EventTenants[i].EventName, eventName) == 0 && String.Compare(EventTenants[i].Service, serviceName) == 0)
                {
                    found = true;
                }
            }

            return found;

        }

        public bool IsEventCustomized(string eventName)
        {
            bool found = false;
            for (int i = 0; i < EventTenants.Count; i++)
            {
                if (String.Compare(EventTenants[i].EventName, eventName) == 0)
                {
                    found = true;
                }
            }
            return found;
        }
    }
}
